'use strict';

/**
 * This is a node script to:
 *  Read mongoDB data dumps from MV team
 *      (markers-object_versions.json & markers-tag_whitelist.json)
 *
 *  Convert dump files into js objects
 *  Merge objects & tags
 *  Extract necessary data from objects to create blipp objects
 *  Output blipp objects as json to adexchange/data/broadmatch-objects.json
 */

var fs = require('fs');

var files = {
    objects : process.argv[2] || 'markers-object_versions.json',
    tags    : process.argv[3] || 'markers-tag_whitelist.json',
    output  : process.argv[4] || 'broadmatch-objects.json'
};


fs.readFile(files.objects, 'utf8', function (err, objectsData) {
    if (err) {
        return console.log(err);
    }

    fs.readFile(files.tags, 'utf8', function (err, tagsData) {
        if (err) {
            return console.log(err);
        }

        main(
            objectify(objectsData),
            objectify(tagsData)
        );
    });
});


/**
 *
 * @param bmObjects
 * @param bmTags
 */
function main(bmObjects, bmTags) {
    var blippData = [];

    addTagsToObjects(bmObjects, bmTags);
    constructBlipps(blippData, bmObjects);

    fs.writeFile(files.output, JSON.stringify(blippData), function(err){
        if (err) {
            return console.log(err);
        }

        console.log('file written: ', files.output);
    });
}


function constructBlipps(blippData, bmObjects) {
    var re = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);

    // latest version?
    bmObjects.forEach(function(_object){
        // 'latest' version, by key..
        var version;
        for (var i = _object.versions.length - 1; i >= 0; i--) {
            if (!_object.versions[i].code) {
                version = _object.versions[i];
                break;
            }
        }

        _object.latestversion = version;

        if (_object.latestversion &&
            _object.latestversion.blipphash &&
            re.test(_object.latestversion.blipphash)) {

            _object.isvalid = true;

            blippData.push({
                BlippID: _object.latestversion.blipphash,
                MainMarkerID: _object.latestversion.markerid,
                Tags: _object.tags,
                Name: _object.name,
                ObjectID: _object.id,
                BlippWiseID: _object.bwid
            });

            console.log('success:', _object.name);

        } else {
            console.warn('failed:', _object.name);

        }
    });
}

/**
 * Merge Objects & Tags
 * Add tags property to object
 * @param bmObjects
 * @param bmTags
 */
function addTagsToObjects(bmObjects, bmTags) {
    var bmObjectMap = {};

    // attach tags[] to our bmObjects
    bmObjects.forEach(function(_val){
        _val.tags = [];
        _val.tags.push(_val.name);
        bmObjectMap[_val.id] = _val;
    });

    // append tags to the bmObjects
    bmTags.forEach(function(_val){
        if (bmObjectMap[_val.value]) {
            bmObjectMap[_val.value].tags.push(_val.tag.toLowerCase());
        }
    });

}

/**
 * Create an array of js objects from the mongo data dump
 * @param _data
 * @returns {Array}
 */
function objectify(_data) {
    var arr = [];

    _data = _data.split('\n');

    _data.forEach(function(_val){
        _val = JSON.parse(_val);
        arr.push(_val);
    });

    return arr;
}